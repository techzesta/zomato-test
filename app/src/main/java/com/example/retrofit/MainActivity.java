package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.Toast;

import com.example.retrofit.adapter.RestaurantAdapter;
import com.example.retrofit.model.RestaurantModel;
import com.example.retrofit.model.SearchModel;
import com.example.retrofit.retrofit.ApiInterface;
import com.example.retrofit.retrofit.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    SearchView searchView;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onDeclare();
        searchOperation();
    }

    private void searchOperation() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                hitApi(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private void onDeclare() {
        searchView = (SearchView) findViewById(R.id.activity_main_sv);
        recyclerView = (RecyclerView) findViewById(R.id.activity_main_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this,
                LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void hitApi(String query) {
        final ApiInterface apiInterface = RetrofitInstance.getService().create(ApiInterface.class);
        Call<SearchModel> call = apiInterface.loadSearchModel(RetrofitInstance.ZOMATO_KEY,query);
        call.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                if(response.isSuccessful()){
                   List<RestaurantModel> restaurantList = response.body().getRestaurants();
                   Log.e("Test", "Successful");
                    if(restaurantList.size() > 0){
                        Log.e("Test", restaurantList.toString());
                    }
                }else{
                    Log.e("Test", "Failed");
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}