
package com.example.retrofit.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class RestaurantModel {

    @SerializedName("average_cost_for_two")
    private String mAverageCostForTwo;
    @SerializedName("cuisines")
    private String mCuisines;
    @SerializedName("name")
    private String mName;
    @SerializedName("thumb")
    private String mThumb;

    public String getAverageCostForTwo() {
        return mAverageCostForTwo;
    }

    public void setAverageCostForTwo(String averageCostForTwo) {
        mAverageCostForTwo = averageCostForTwo;
    }

    public String getCuisines() {
        return mCuisines;
    }

    public void setCuisines(String cuisines) {
        mCuisines = cuisines;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getThumb() {
        return mThumb;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }


}
