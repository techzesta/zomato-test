
package com.example.retrofit.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchModel {

    @SerializedName("restaurants")
    private List<RestaurantModel> mRestaurantModels;

    public List<RestaurantModel> getRestaurants() {
        return mRestaurantModels;
    }

    public void setRestaurants(List<RestaurantModel> restaurantModels) {
        mRestaurantModels = restaurantModels;
    }
}
