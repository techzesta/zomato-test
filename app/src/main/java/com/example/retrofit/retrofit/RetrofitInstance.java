package com.example.retrofit.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance{

    public static final String BASE_URL = "https://developers.zomato.com/api/v2.1/";
    public static final String ZOMATO_KEY = "f15d0f50905be405b967ca0c71bc3b84";

    public static Retrofit retrofit = null;

    public static Retrofit getService(){
        // Retrofit Builder
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
