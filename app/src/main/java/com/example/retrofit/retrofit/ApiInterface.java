package com.example.retrofit.retrofit;

import com.example.retrofit.model.SearchModel;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("search")
    Call<SearchModel> loadSearchModel(@Header("user-key") String key, @Query("q") String query);
}
