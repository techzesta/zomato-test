package com.example.retrofit.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofit.R;
import com.example.retrofit.model.RestaurantModel;

import java.util.List;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.ViewHolder>{

    private List<RestaurantModel> restaurantArrayList;

    private Context context;

    public RestaurantAdapter(List<RestaurantModel> restaurantArrayList){
        this.restaurantArrayList = restaurantArrayList;
    }

    @NonNull
    @Override
    public RestaurantAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_restaurant_adapter, parent, false);
        return new RestaurantAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantAdapter.ViewHolder holder, int position) {
        final RestaurantModel restaurantModel = restaurantArrayList.get(position);
        if(!TextUtils.isEmpty(restaurantModel.getName())) {
            holder.mName.setText(restaurantModel.getName());
        }
        if(!TextUtils.isEmpty(restaurantModel.getName())) {
            holder.mCuisines.setText(restaurantModel.getCuisines());
        }
        if(!TextUtils.isEmpty(restaurantModel.getName())) {
            holder.mAvgCost.setText(restaurantModel.getAverageCostForTwo());
        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView mName;
        private TextView mCuisines;
        private TextView mAvgCost;
        private TextView mUrl;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.activity_restaurant_tv1);
            mCuisines = itemView.findViewById(R.id.activity_restaurant_tv2);
            mAvgCost = itemView.findViewById(R.id.activity_restaurant_tv3);
        }
    }
}
